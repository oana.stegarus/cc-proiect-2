const jwt = require('jsonwebtoken');

const {
    ServerError
} = require('../../errors/index');

const options = {
    issuer: "CloudComputing",
    subject: "ProiectCC",
    audience: "EGOV"
};

const {
    getSecret
} = require('docker-secret');


const verifyAndDecodeData = async (token) => {
    try {
        const decoded = await jwt.verify(token, getSecret('jwtKey'), options);
        return decoded;
    } catch (err) {
        console.trace(err);
        throw new ServerError("Eroare la decriptarea tokenului!", 400);
    }
};

const authorizeAndExtractToken = async (req, res, next) => {
    try {
        if (!req.headers.authorization) {
            throw new ServerError('Lipseste headerul de autorizare!', 403);
        }
        const token = req.headers.authorization.split(" ")[1]; // se separa dupa " " deoarece este de forma: Bearer 1wqeiquqwe0871238712qwe


        const decoded = await verifyAndDecodeData(token);

        req.state = {
            decoded
        };

        next();
    } catch (err) {
        next(err);
    }
};

module.exports = {
    authorizeAndExtractToken
};

