const Router = require("express").Router();

const { sendRequest } = require("../http-client");

Router.post("/register", async (req, res) => {
    const {username, email, password} = req.body;

    const postRegister = {
        url: `http://${process.env.AUTHENTICATION_SERVICE_API_ROUTE}/register`,
        method: "POST",
        data: {
            username,
            email,
            password,
        },
    };

    const id = await sendRequest(postRegister);

    res.json({ id });
});

Router.post("/login", async (req, res) => {
    const {username, password} = req.body;

    const postRegister = {
        url: `http://${process.env.AUTHENTICATION_SERVICE_API_ROUTE}/login`,
        method: "POST",
        data: {
            username,
            password
        },
    };

    const token = await sendRequest(postRegister);

    res.json({ token });
});

module.exports = Router;
