const Router = require('express').Router();

const CarsRoute = require('./cars.js');

const UsersRoute = require('./users.js');

Router.use('/cars', CarsRoute);
Router.use('/users', UsersRoute);

module.exports = Router;
