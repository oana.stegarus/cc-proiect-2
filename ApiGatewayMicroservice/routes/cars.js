const Router = require("express").Router();

const { sendRequest } = require("../http-client");

const {authorizeAndExtractToken} = require('../security/Jwt/index');

Router.get("/", authorizeAndExtractToken, async (req, res) => {
  console.info(`Forwarding request for get cars ...`);

  const getCarsRequest = {
    url: `http://${process.env.CARS_SERVICE_API_ROUTE}`,
  };

  const cars = await sendRequest(getCarsRequest);

  res.json(cars);
});

Router.post("/", authorizeAndExtractToken, async (req, res) => {
  const { model, car_name, horse_power, an, indice_poluare } = req.body;

  const postCarRequest = {
    url: `http://${process.env.CARS_SERVICE_API_ROUTE}`,
    method: "POST",
    data: {
        model,
        car_name,
        horse_power,
        an,
        indice_poluare
    },
  };

  const id = await sendRequest(postCarRequest);

  res.json({ id });
});

module.exports = Router;
