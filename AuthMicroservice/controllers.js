const Router = require('express').Router();

const {
    ServerError
} = require('./errors');

const {
    addUser,
    authenticate
} = require('./services.js');



Router.post('/register', async (req, res) => {

    const {
        username,
        email,
        password
    } = req.body;

    if (!username) {
        throw new ServerError('No username provided!', 400);
    }

    if (!email) {
        throw new ServerError('No email provided!', 400);
    }

    if (!password) {
        throw new ServerError('No password provided!', 400);
    }

    const id = await addUser(username, email, password);

    res.json(id);
});

Router.post('/login', async (req, res) => {

    const {
        username,
        password
    } = req.body;

    if (!username) {
        throw new ServerError('No username provided!', 400);
    }


    if (!password) {
        throw new ServerError('No password provided!', 400);
    }

    const token = await authenticate(username, password);

    res.json(token);
});

module.exports = Router;