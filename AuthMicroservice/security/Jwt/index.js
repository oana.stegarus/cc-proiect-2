const jwt = require('jsonwebtoken');

const {
    ServerError
} = require('../../errors/index');

const {
    getSecret
} = require('docker-secret');

const options = {
    issuer: "CloudComputing",
    subject: "ProiectCC",
    audience: "EGOV"
}

const generateToken = async (payload) => {

    const token = jwt.sign(payload, getSecret('jwtKey'), options);
    return token;
};

module.exports = {
    generateToken
};

