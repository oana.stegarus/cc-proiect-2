const {
    sendRequest
} = require('./http-client');

const {
    generateToken,
} = require('./security/Jwt/index');


const {
    hash,
    compare
} = require('./security/Password/index');


const addUser = async (username, email, password) => {

    // const encryptedPassword = await hash(password);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/register`,
        method: 'POST',
        data: {
            username,
            email,
            password
        }
    }

    const id = await sendRequest(options);

    return id;
};

const authenticate = async (username, password) => {

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/login`,
        method: 'POST',
        data: {
            username,
            password
        }
    }

    const user = await sendRequest(options);

    // if (await compare(password, user.password) == false) {
    //     throw new Error('Parola gresita');
    // }
    const token = await generateToken( {
        "userId": user.id,
    });

    return token;
};

module.exports = {
    addUser,
    authenticate
}