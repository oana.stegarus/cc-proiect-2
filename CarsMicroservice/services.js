const {
    sendRequest
} = require('./http-client');

const getCars = async () => {
    console.info(`Sending request to IO for all cars ...`);
    
    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/cars`
    }

    const books = await sendRequest(options);

    return books;
};

const addCar = async (model, car_name, horse_power, an, indice_poluare) => {

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/cars`,
        method: 'POST',
        data: {
            model,
            car_name,
            horse_power,
            an,
            indice_poluare
        }
    }

    const id = await sendRequest(options);

    return id;
};  

module.exports = {
    getCars,
    addCar
}