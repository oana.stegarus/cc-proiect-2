const Router = require('express').Router();

const {
    ServerError
} = require('./errors');

const {
    getCars,
    addCar
} = require('./services.js');

Router.get('/', async (req, res) => {
    
    const cars = await getCars();

    res.json(cars);
});

Router.post('/', async (req, res) => {

    const {
        model,
        car_name,
        horse_power,
        an,
        indice_poluare
    } = req.body;

    if (!model) {
        throw new ServerError('No model provided!', 400);
    }

    if (!car_name) {
        throw new ServerError('No car name provided!', 400);
    }

    if (!horse_power) {
        throw new ServerError('No horse power provided!', 400);
    }
    if (!an) {
        throw new ServerError('No year provided!', 400);
    }
    if (!indice_poluare) {
        throw new ServerError('No indice poluare provided!', 400);
    }

    const id = await addCar(model, car_name, horse_power, an, indice_poluare);

    res.json(id);
});

module.exports = Router;
