const Router = require('express').Router();

const {
    getCars,
    addCar
} = require('./services.js');

Router.get('/', async (req, res) => {
    
    const cars = await getCars();

    res.json(cars);
});


Router.post('/', async (req, res) => {

    const {
        model,
        car_name,
        horse_power,
        an,
        indice_poluare
    } = req.body;

    const id = await addCar(model, car_name, horse_power, an, indice_poluare);

    res.json(id);
});

module.exports = Router;