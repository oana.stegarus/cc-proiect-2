const {
    query
} = require('../data');

const getCars = async () => {
    console.info(`Getting all cars ...`);

    const cars = await query("SELECT * FROM car");

    return cars;
};


const addCar = async (model, car_name, horse_power, an, indice_poluare) => {

    const cars =  await query('INSERT INTO car (model, car_name, horse_power, an, indice_poluare) VALUES($1, $2, $3, $4, $5)', [model, car_name, horse_power, an, indice_poluare]);

    return 5;
};

module.exports = {
    getCars,
    addCar
}