const Router = require('express').Router();

const CarsController = require('./cars/controllers.js');
const UsersController = require('./users/controller.js');

Router.use('/cars', CarsController);
Router.use('/users', UsersController);

module.exports = Router;