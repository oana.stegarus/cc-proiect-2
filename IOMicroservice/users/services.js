const {
    query
} = require('../data');

const {
    ServerError
} = require('../errors/index')

const addUser = async (username, email, password) => {

    const users =  await query('INSERT INTO users (username, email, password) VALUES($1, $2, $3)', [username, email, password]);

    return 5;
};

const authenticate = async (username, email, password) => {

    const result = await query(`SELECT u.id, u.password FROM users u WHERE u.username = $1`, [username]);
    if (result.length === 0) {
        throw new ServerError(`Utilizatorul cu username ${username} nu exista in sistem!`, 400);
    }
    return result[0];
};

module.exports = {
    addUser,
    authenticate
}