const Router = require('express').Router();

const {
    addUser,
    authenticate
} = require('./services.js');



Router.post('/register', async (req, res) => {

    const {
        username,
        email,
        password
    } = req.body;

    const id = await addUser(username, email, password);

    res.json(id);
});

Router.post('/login', async (req, res) => {

    const {
        username,
        password
    } = req.body;

    const user = await authenticate(username, password);

    res.json(user);
});

module.exports = Router;