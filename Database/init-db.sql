-- CREATE TABLE IF NOT EXISTS books (
--     id serial PRIMARY KEY,
--     title varchar NOT NULL,
--     author varchar NOT NULL,
--     genre varchar NOT NULL
-- );

CREATE TABLE IF NOT EXISTS users (
    id serial PRIMARY KEY,
    username varchar NOT NULL,
    email varchar NOT NULL,
    password varchar NOT NULL,
    created_at timestamp
);
CREATE TABLE IF NOT EXISTS car (
    id serial PRIMARY key,
    model varchar NOT NULL,
    car_name varchar NOT NULL,
    horse_power integer NOT NULL,
    an integer NOT NULL,
    indice_poluare varchar NOT NULL
)